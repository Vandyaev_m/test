package com.example.demo;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface MongoRep extends MongoRepository<SomeModel, String> {

}
