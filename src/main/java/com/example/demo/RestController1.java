package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestController1 {

	@Autowired
	private MongoRep mongoRep;
	
	@GetMapping("/")
	public String test() {
		return "all right";
	}
	
	@RequestMapping(value = "addData/{testStr}")
	public void addData(@PathVariable String testStr) {
		SomeModel model = new SomeModel();
		model.setTestStr(testStr);
		mongoRep.save(model);
	}

	@RequestMapping(value = "getData")
	public List<SomeModel> getData(){
		return mongoRep.findAll();
	}
	
}
